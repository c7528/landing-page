import NextAuth, { User } from "next-auth";
import Providers from "next-auth/providers";
import Adapters from "next-auth/adapters";
import { sendThankYouMail } from "@src/lib/mail";

export default NextAuth({
  providers: [
    // OAuth authentication providers
    // Providers.Apple({
    //   clientId: process.env.APPLE_ID,
    //   clientSecret: process.env.APPLE_SECRET,
    // }),
    // Providers.Google({
    //   clientId: process.env.GOOGLE_CLIENT_ID,
    //   clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    // }),
    Providers.Email({
      server: process.env.EMAIL_SERVER,
      from: process.env.EMAIL_FROM,
      sendVerificationRequest({
        identifier: email,
        url,
        provider: { server, from },
      }) {
        // sendThankYouMail(email);
      },
    }),
  ],
  database: process.env.MONGODB_URI,
  theme: "auto", // "auto" | "dark" | "light"
  adapter: Adapters.TypeORM.Adapter(process.env.MONGODB_URI || "", {
    models: {
      ...Adapters.TypeORM.Models,
      User: {
        model: Adapters.TypeORM.Models.User.model,
        schema: {
          ...Adapters.TypeORM.Models.User.schema,
          columns: {
            ...Adapters.TypeORM.Models.User.schema.columns,
            role: {
              type: "varchar",
              nullable: true,
            },
          },
        },
      },
    },
  }),
  // assign default role on user creation
  events: {
    createUser: async (user: User) => {
      //await setUserRole(user.id, "guest");
      // updateUser(getEnrichedUser(user));
      //await setUserRole(user.id, "guest");
      // if (user.email) await setUserRole(user.email, "user");
    },
  },
  // enrich session data
  callbacks: {
    // session: async (session: ExtendedSession, user: ExtendedUser) => {
    //   if (session && session.user) {
    //     session.user.role = user.role; // inject role into session
    //     // get all permissions
    //     const grants = accessControl.getGrants();
    //     // expose only the current role permissions
    //     session.user.permissions =
    //       user.role in grants ? { [user.role]: grants[user.role] } : {};
    //   }
    //   return Promise.resolve(session);
    // },
  },

  debug: false,
  session: {
    //   // Use JSON Web Tokens for session instead of database sessions.
    //   // This option can be used with or without a database for users/accounts.
    //   // Note: `jwt` is automatically set to `true` if no database is specified.
    jwt: false,

    //   // Seconds - How long until an idle session expires and is no longer valid.
    // maxAge: 30 * 24 * 60 * 60, // 30 days

    //   // Seconds - Throttle how frequently to write to database to extend a session.
    //   // Use it to limit write operations. Set to 0 to always update the database.
    //   // Note: This option is ignored if using JSON Web Tokens
    //   // updateAge: 24 * 60 * 60, // 24 hours
  },

  // TODO: add secret
  // JSON Web tokens are only used for sessions if the `jwt: true` session
  // option is set - or by default if no database is specified.
  // https://next-auth.js.org/configuration/options#jwt
  // jwt: {
  // A secret to use for key generation (you should set this explicitly)
  // secret: 'INp8IvdIyeMcoGAgFGoA61DdBglwwSqnXJZkgz8PSnw',
  // Set to true to use encryption (default: false)
  // encryption: true,
  // You can define your own encode/decode functions for signing and encryption
  // if you want to override the default behaviour.
  // encode: async ({ secret, token, maxAge }) => {},
  // decode: async ({ secret, token, maxAge }) => {},
  // },
  pages: {
    signIn: "/",
    // verifyRequest: "/thank-you", // (used for check email message)
    // newUser: "/tell-others", // New users will be directed here on first sign in (leave the property out if not of interest)
  },
});
