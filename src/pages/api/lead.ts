import type { NextApiRequest, NextApiResponse } from "next";
import nc from "next-connect";

import { createContact } from "@src/lib/hubspot-utils";
import { createUnverifiedUser } from "@src/lib/user";
import connectDB from "@src/middleware/mongodb";
import { sendThankYouEmail } from "@src/lib/mail";

const handler = nc().post(
  async (req: NextApiRequest, res: NextApiResponse<any>) => {
    // update user profile in hubspot
    try {
      const { email, referrer } = req.body;
      createContact(email);
      const id = await createUnverifiedUser(email, referrer);
      //await sendThankYouMail(email);
      await sendThankYouEmail(email, id);
      res.status(200).send({ referrer: id });
    } catch (err) {
      res.status(200);
    }
  }
);

export default connectDB(handler);
