import { getUser, getUsers, updateUser } from "@src/lib/db-utils";
import type { NextApiRequest, NextApiResponse } from "next";
import connectDB from "@src/middleware/mongodb";
import { IUserDocument } from "@src/models/user";
import nc from "next-connect";
import { userAuth } from "@src/middleware/user-auth";
import {
  checkAccess,
  getPossessionFromBody,
  getPossessionFromQuery,
} from "@src/middleware/check-access";

// type Data = {
//   results: {
//     [x: string]: {
//       [x: string]: number;
//     }[];
//   }[];
// };

const handler = nc()
  .use(userAuth) // injects session into req.session
  .get(
    checkAccess("users", "read:own", getPossessionFromQuery),
    async (req: NextApiRequest, res: NextApiResponse<any>) => {
      // get user profile from DB
      try {
        // TODO: verify permissions
        const email = req.query.email as string;

        if (email) {
          const user = await getUser(email);
          res.status(200).json({ user });
        } else {
          res.status(404).send("not found");
        }
      } catch (err) {
        res.status(500).send("faild to retrieve user");
      }
    }
  )
  .post(
    checkAccess("users", "update:own", getPossessionFromBody),
    async (req: NextApiRequest, res: NextApiResponse<any>) => {
      // update user profile in DB
      try {
        const user: IUserDocument = req.body.user;

        const updatedUser = await updateUser(user);
        res.send(updatedUser);
      } catch (err) {
        res.status(500).send("error");
      }
    }
  );

export default connectDB(handler);
