import type { AppProps } from "next/app";
import React, { useEffect, useState } from "react";
import { Provider } from "next-auth/client";
import { useRouter } from "next/router";
import { ThemeProvider } from "@mui/material/styles";
import { darkTheme, lightTheme } from "@src/styles/theme";
import { BasicLayout } from "@src/common/basic-layout";

function App({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const [isDarkTheme, setIsDarkTheme] = useState(false);

  useEffect(() => {
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles && jssStyles.parentElement) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);
  return (
    <ThemeProvider theme={isDarkTheme ? darkTheme : lightTheme}>
      <div suppressHydrationWarning>
        {typeof window === "undefined" ? null : (
          <Provider session={pageProps.session}>
            <BasicLayout
              toggleTheme={() => {
                setIsDarkTheme((theme) => !theme);
              }}
              isDarkTheme={isDarkTheme}
            >
              <Component {...pageProps} key={router.asPath} />
            </BasicLayout>
          </Provider>
        )}
      </div>
    </ThemeProvider>
  );
}
export default App;
