import { Button } from "@mui/material";
import router from "next/dist/client/router";
import React, { useCallback } from "react";
import { Typography } from "@mui/material";
import { useDispatch, useCurrentUser } from "@src/context/context";
import { updateUserAcceptTerms } from "@src/context/actions";

export const AcceptTerms: React.FC = () => {
  const dispatch = useDispatch();
  const user = useCurrentUser();
  const acceptTerms = useCallback(async () => {
    if (user) {
      updateUserAcceptTerms(user)(dispatch);

      router.push(`/`, undefined, {
        shallow: true,
      });
    }
  }, [user, dispatch]);

  return user && !user.acceptedTerms ? (
    <>
      <Typography variant="body1" gutterBottom>
        CLO is committed to protecting and respecting your privacy, and we’ll
        only use your personal information to administer your account and to
        provide the products and services you requested from us. From time to
        time, we would like to contact you about our products and services, as
        well as other content that may be of interest to you. If you consent to
        us contacting you for this purpose, please tick below to say how you
        would like us to contact you: I agree to receive other communications
        from CLO. You can unsubscribe from these communications at any time. For
        more information on how to unsubscribe, our privacy practices, and how
        we are committed to protecting and respecting your privacy, please
        review our Privacy Policy. By clicking submit below, you consent to
        allow CLO to store and process the personal information submitted above
        to provide you the content requested.
      </Typography>
      <Button variant="outlined" data-hook="accept" onClick={acceptTerms}>
        Accept
      </Button>
    </>
  ) : (
    <></>
  );
};
