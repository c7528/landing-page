import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@mui/styles";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Popover from "@mui/material/Popover";
import IconButton from "@mui/material/IconButton";
import { Menu as MenuIcon } from "@mui/icons-material";
import { Link, Route, Switch } from "react-router-dom";

import {
  usePopupState,
  bindTrigger,
  bindPopover,
} from "material-ui-popup-state/hooks";
import { Theme } from "@mui/material/styles";
import { routes } from "@src/common/routing";
import { ListItemText, MenuItem, MenuList } from "@mui/material";

const styles = (theme: Theme) => ({
  typography: {
    margin: theme.spacing(2),
  },
});

const PopoverPopupState = () => {
  const popupState = usePopupState({
    variant: "popover",
    popupId: "demoPopover",
  });
  return (
    <div>
      <IconButton
        size="large"
        edge="start"
        color="inherit"
        aria-label="menu"
        sx={{ mr: 2 }}
        {...bindTrigger(popupState)}
      >
        <MenuIcon />
      </IconButton>

      <Popover
        {...bindPopover(popupState)}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <MenuList>
          {routes.map((route, index) => (
            <MenuItem component={Link} to={route.path} key={index}>
              {route.menu()}
            </MenuItem>
          ))}
        </MenuList>
      </Popover>
    </div>
  );
};

PopoverPopupState.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PopoverPopupState);
