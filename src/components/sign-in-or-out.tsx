import { useSession } from "next-auth/client";
import React from "react";
import { Loader } from "@src/components/loader";
import { SignIn } from "@src/components/sign-in";
import { SignOut } from "@src/components/sign-out";

export const SignInOrOut = () => {
  const [session, loading] = useSession();
  if (loading) return <Loader />;
  return session ? <SignOut /> : <SignIn />;
};
