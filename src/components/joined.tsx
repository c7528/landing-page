import React, { useCallback, useEffect } from "react";
import {
  Button,
  Container,
  Stack,
  TextField,
  Typography,
  Box,
} from "@mui/material";
import { Fireworks } from "fireworks-js/dist/react";

import { useWindowSize } from "react-use";
import { useCopyToClipboard } from "react-use";
import { ContentCopy } from "@mui/icons-material";
import { useRouter } from "next/router";

export const Joined: React.FC<{ referCode: string }> = ({ referCode }) => {
  const router = useRouter();
  const [state, copyToClipboard] = useCopyToClipboard();
  const referrerUrl = `theclo.co/?r=${referCode}`;

  return (
    <>
      <Fireworks
        style={{
          top: 0,
          left: 0,
          width: "100%",
          height: "100%",
          position: "fixed",
          zIndex: -1,
        }}
      />

      <Box minHeight="80vh" display="flex" alignItems="center">
        <Container maxWidth="sm">
          <Typography variant="h3">Smart move!</Typography>
          <br />
          <Typography>
            We&apos;ve added your email to the CLO waiting list.
          </Typography>
          <br />
          <Typography>
            The first 5,000 users get priority access to all the new features
            and exclusive giveaways.
            <br />
            <br /> Ask your friends and collegues to join the CLO waitlist using
            your unique link below
            <br />
          </Typography>
          <Stack>
            <Stack direction="row" pt={2}>
              <TextField
                value={referrerUrl}
                disabled
                fullWidth
                variant="filled"
                size="small"
              />

              <Button
                onClick={() => copyToClipboard(referrerUrl)}
                variant="contained"
              >
                <ContentCopy />
              </Button>
            </Stack>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => router.reload()}
            >
              Submit another email
            </Button>
            {state.error ? (
              <Typography variant="caption">
                Unable to copy value: {state.error.message}
              </Typography>
            ) : (
              state.value && <Typography variant="caption">Copied!</Typography>
            )}
          </Stack>
        </Container>
      </Box>
    </>
  );
};
