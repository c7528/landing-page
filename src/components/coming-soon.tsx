import { Box, Container, Typography } from "@mui/material";
import { useSpring, animated, useTransition, config } from "react-spring";
import React, { useEffect, useState } from "react";

export const ComingSoon: React.FC = ({ children }) => {
  const [list, setList] = useState(["COMING SOON"] as string[]);

  const listTransitions = useTransition(list, {
    config: config.gentle,
    from: { opacity: 0, transform: "translate3d(-25%, 0px, 0px)" },
    enter: { opacity: 1, transform: "translate3d(0%, 0px, 0px)" },
    leave: { opacity: 0, height: 0, transform: "translate3d(25%, 0px, 0px)" },
    keys: list.map((item, index) => index),
    delay: 300,
  });

  const useOpacity = (delay: number) =>
    useSpring({
      opacity: 1,
      from: { opacity: 0 },
      delay,
    });

  const AnimatedTypography = animated(Typography);
  const AnimatedBox = animated(Box);

  // useEffect(() => {
  //   setTimeout(() => setList((list) => list.concat("SOON")), 300);
  // }, []);

  return (
    <>
      <Box
        height="70vh"
        display="flex"
        flexDirection="column"
        alignItems="start"
        justifyContent="center"
        p={2}
      >
        <Container maxWidth="md">
          <Box minHeight="15ch" sx={{ display: { xs: "none", sm: "block" } }}>
            {/* {listTransitions((styles, item) => (
              <AnimatedTypography align="center" style={styles} variant="h1">
                {item}
              </AnimatedTypography>
            ))} */}
            <Typography align="center" variant="h1">
              COMING SOON
            </Typography>
          </Box>
          <Box sx={{ display: { xs: "block", sm: "none" } }}>
            {/* {listTransitions((styles, item) => (
              <AnimatedTypography align="center" style={styles} variant="h2">
                {item}
              </AnimatedTypography>
            ))} */}
            <Typography variant="h2" align="center">
              COMING SOON
            </Typography>
          </Box>
          <Box p={2}>
            {/* <AnimatedBox style={useOpacity(1000)} p={2}> */}
            <Typography variant="h4" align="center">
              A fresh career development tool for women
            </Typography>
            <Container maxWidth="sm">{children}</Container>
            {/* </AnimatedBox> */}
          </Box>
        </Container>
      </Box>
    </>
  );
};
