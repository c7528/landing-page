import { Button } from "@mui/material";
import { signIn } from "next-auth/client";
import React from "react";

export const SignIn = () => (
  <Button color="inherit" onClick={() => signIn()}>
    Sign in
  </Button>
);
