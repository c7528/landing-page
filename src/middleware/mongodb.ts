import mongoose from "mongoose";
import { NextApiRequest, NextApiResponse } from "next";
import { connectToDatabase } from "../lib/db-utils";

const connectDB =
  (handler: (req: NextApiRequest, res: NextApiResponse<any>) => any) =>
  async (req: NextApiRequest, res: NextApiResponse<any>) => {
    if (mongoose.connections[0].readyState) {
      // Use current db connection
      return handler(req, res);
    }
    // Use new db connection
    await connectToDatabase();
    return handler(req, res);
  };

export default connectDB;
