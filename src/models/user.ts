import mongoose, { Document, model, Model, Schema } from "mongoose";

export interface IUser {
  name: string;
  email?: string | null;
  image?: string | null;
  emailVerified?: boolean;
  acceptedTerms?: boolean;
  acceptedTermsAt?: number;
  role: string;
  hubspotId?: string | null;
  referrer?: string;
}
export interface IUserDocument extends Document, IUser {}

const UserSchema: Schema = new Schema({
  name: {
    type: String,
  },
  role: {
    type: String,
    defualt: "guest",
  },
  email: {
    type: String,
  },
  image: {
    type: String,
  },
  emailVerified: {
    type: Date,
    default: null,
  },
  acceptedTerms: {
    type: Boolean,
    default: false,
  },
  acceptedTermsAt: {
    type: Date,
    default: Date.now,
  },
  hubspotId: {
    type: String,
    default: null,
  },
  referrer: { type: String },
});

export const User: Model<IUserDocument> =
  mongoose.models.User || model("User", UserSchema);
