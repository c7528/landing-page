import { BrowserRouter as Router } from "react-router-dom";

export const routes = [
  {
    path: "/",
    exact: true,
    menu: () => "coming soon",
    main: () => <></>,
  },
];

export const Routing: React.FC = ({ children }) => {
  // TODO: add header
  return <Router>{children}</Router>;
};
