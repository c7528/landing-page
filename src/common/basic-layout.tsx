import Head from "next/head";
import React, { useEffect } from "react";
import { Routing, routes } from "./routing";
import { Switch, Route } from "react-router-dom";
import { Box, Paper, Stack, Theme } from "@mui/material";
import { makeStyles } from "@mui/styles";
import Link from "next/link";
import { LightLogo } from "@src/components/light-logo";
import { Logo } from "@src/components/logo";

import { useRouter } from "next/router";
import { fetcher } from "@src/lib/utils";
import { LightMode, DarkMode } from "@mui/icons-material";
import { IconButton } from "@mui/material";
import { SkyBackground } from "./sky-background";

const useStyles = makeStyles((theme: Theme) => ({
  "@global": {
    "*::-webkit-scrollbar": {
      width: "0.4em",
    },
    "*::-webkit-scrollbar-track": {
      "-webkit-box-shadow": "inset 0 0 6px rgba(0,0,0,0.00)",
    },
    "*::-webkit-scrollbar-thumb": {
      backgroundColor: theme.palette.mode === "dark" ? "#38b1eb" : "#6C4DEA",
      outline:
        theme.palette.mode === "dark"
          ? "1px solid #38b1eb"
          : "1px solid #6C4DEA",
      borderRadius: "4px",
    },
    body: {
      margin: 0,
      overflowY: "auto",
      overflowX: "hidden",
      width: "100%",
    },
  },
  root: {
    width: "100wh",
    height: "100vh",
    // position: "relative",
    "& video": {
      objectFit: "cover",
    },

    margin: 0,
  },

  title: {
    paddingBottom: theme.spacing(4),
  },
}));

interface BasicLayoutProps {
  toggleTheme: () => void;
  isDarkTheme: boolean;
}
export const BasicLayout: React.FC<BasicLayoutProps> = ({
  children,
  toggleTheme,
  isDarkTheme,
}) => {
  const router = useRouter();
  const slug = router.pathname;
  const icon = isDarkTheme ? <LightMode /> : <DarkMode />;

  useEffect(() => {
    const registerView = async () => {
      const pageName = slug.length === 1 ? "/home" : slug;
      await fetcher(`/api/views${pageName}`, "POST");
    };

    registerView();
  }, [slug]);

  const classes = useStyles();
  return (
    <>
      <Head>
        <title>CLO</title>
      </Head>

      <Routing>
        <Paper>
          <SkyBackground isDarkTheme={isDarkTheme}>
            <Stack direction="column" sx={{ zIndex: 1 }}>
              <Box height="6rem">
                <Stack
                  direction="row"
                  justifyContent="space-between"
                  alignItems="flex-start"
                  width="98vw"
                >
                  <Link href="/">
                    <a>{isDarkTheme ? <LightLogo /> : <Logo />}</a>
                  </Link>
                  <IconButton
                    edge="end"
                    color="inherit"
                    aria-label="mode"
                    onClick={toggleTheme}
                  >
                    {icon}
                  </IconButton>
                </Stack>
              </Box>
              <Switch>
                {routes.map((route, index) => (
                  <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    children={<route.main />}
                  />
                ))}
              </Switch>
              {children}
            </Stack>
          </SkyBackground>{" "}
        </Paper>
      </Routing>
    </>
  );
};
