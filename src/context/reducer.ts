import {
  UsersState,
  Actions,
  ACCEPT_TERMS,
  INIT_STATE,
  UPDATE_CURRENT_USER,
  UPDATE_USERS,
} from "./types";

export const userReducer = (state: UsersState, action: Actions) => {
  switch (action.type) {
    case ACCEPT_TERMS:
      return { ...state, user: action.user };
    case INIT_STATE:
      return action.state;
    case UPDATE_CURRENT_USER:
      return { ...state, user: action.user, loading: false };
    default:
      throw new Error();
  }
};
