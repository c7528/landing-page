import axios, { AxiosRequestConfig } from "axios";
import * as hubspot from "@hubspot/api-client";

const hubspotClient = new hubspot.Client({
  accessToken: process.env.HUBSPOT_ACCESS_TOKEN,
});

export const createContact = async (email: string) => {
  try {
    const res = await hubspotClient.crm.contacts.basicApi.create({
      properties: {
        email,
        firstname: email,
        // referrer: "none",
      },
    });
    return res.body.id;
  } catch (e) {
    console.log(e);
    return null;
  }
};

export const createOrUpdateContactWithReferrere = async (
  email: string,
  refferrer?: string
) => {
  try {
    const data = JSON.stringify({
      properties: [
        { property: "referrer", value: refferrer },
        // {
        //   property: "address",
        //   value: refferrer,
        // },
        // { property: "lastname", value: refferrer },
        // { property: "website", value: "http://hubspot.com" },
        // { property: "company", value: "HubSpot" },
        // { property: "phone", value: "555-122-2323" },
        // { property: "address", value: "25 First Street" },
        // { property: "city", value: "Cambridge" },
        // { property: "state", value: "MA" },
        // { property: "zip", value: "02139" },
      ],
    });

    const config: AxiosRequestConfig = {
      method: "post",
      url: `https://api.hubapi.com/contacts/v1/contact/createOrUpdate/email/${email}?hapikey=${process.env.HUBSPOT_API_KEY}`,
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    };

    const response = await axios(config);
    return response.data.id;
  } catch (e) {
    // TODO : add logs
    console.log(e);
  }
};

export const submitForm = async (email: string, referrer?: string) => {
  const url = `https://api.hsforms.com/submissions/v3/integration/submit/:portalId/:formGuid`;
};

export const viewForm = async () => {};
